import Ember from 'ember';

const access_token = "access_token";
const token_type = "token_type";
const expires = "expires_in";
const state = "state";


export default Ember.Route.extend({

  ajax: Ember.inject.service(),

  retrieve: function(accessToken, totalResponse, ajaxObject, callbackFn) {
    if (totalResponse["next"] == null) {
      return Promise.resolve(totalResponse);
    } else {
      return ajaxObject.request(
        totalResponse["next"], {
          headers: {
            Authorization: "Bearer " + accessToken
          }
        }
      ).then(function(subResponse) {
        subResponse["items"] = subResponse["items"].concat(totalResponse["items"]);
        return callbackFn(accessToken, subResponse, ajaxObject, callbackFn);
      });
    }
  },

  retrieveAllPlaylists: function(accessToken) {
    let retrieveFn = this.retrieve;
    const ajaxObject = this.get('ajax');
    return this.get('ajax').request(
      "https://api.spotify.com/v1/me/playlists",{
        headers: {
          Authorization: "Bearer " + accessToken
        }
      }
    ).then(function(response) {
      return retrieveFn(accessToken, response, ajaxObject, retrieveFn);
    }).then(function(totalResponse) {
      return totalResponse["items"];
    });
  },

  model() {

    let hashFragment = window.location.hash.substr(1);
    let hashMap = hashFragment.split("&").reduce(function(map, keyValue) {
      let separated = keyValue.split("=");
      map[separated[0]] = separated[1];
      return map;
    }, {});

    let accessToken = hashMap[access_token];
    return this.retrieveAllPlaylists(accessToken, 0);
  }
});
