import Ember from 'ember';

export default Ember.Controller.extend({
  spotifyAuthorizationUrl: "https://accounts.spotify.com/authorize?client_id=41e1f0113289401e9aedd6a85e736d62&redirect_uri=http:%2F%2Flocalhost:4200&scope=playlist-modify-private%20playlist-modify-public&state=1&response_type=token"
})

// http://localhost:4200/#access_token=BQBZTIyA9cBmuhbfKx4l0VfXmkfh5OwW4YlcWdXmXiLp4jf0T08mg0mPFu_Xffy6ikDeCoXA2Ac61OHsmzY1C2VlWzD-JgB8mYq_eJ9WoNqsgKeGJO_MCApz4ugXInCPYJBimU3GWjj4R7JZJDj3BMNwX0tgLLeyvMoi2_erzEG1feyigUy68eHQ9QFgJBMF58cChs2W9sMCSbP-aYR4gegjYQ-mnDkcPmBPtGk&token_type=Bearer&expires_in=3600&state=1
